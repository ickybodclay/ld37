﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float Speed;

	private Rigidbody2D rb2d;

	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	void Update () {
		if (!enabled) return;

		HandleInput();
	}

	private Vector2 force;
	private void HandleInput() {
		if (Input.GetKey(KeyCode.LeftArrow));

		float xSpeed = Input.GetAxisRaw("Horizontal") * Time.deltaTime * Speed;
		float ySpeed = Input.GetAxisRaw("Vertical") * Time.deltaTime * Speed;

		//transform.Translate(xSpeed, ySpeed, 0);
		
		force.x = xSpeed;
		force.y = ySpeed;
		rb2d.AddForce(force, ForceMode2D.Impulse);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.collider.tag == "Wall") {
			rb2d.isKinematic = true;
			//rb2d.velocity = Vector3.zero;
			//rb2d.angularVelocity = 0f;
		}
  	}
}
